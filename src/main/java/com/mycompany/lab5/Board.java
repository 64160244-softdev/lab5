/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab5;

/**
 *
 * @author informatics
 */
public class Board {
    private char[][] board = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    private Player player1,player2,currentPlayer;

    public Board(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = player1;
    }

    public char[][] getBoard() {
        return board;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public boolean setBoard(int row,int col) {
        if(board[row][col] == '-'){
            board[row][col] = currentPlayer.getSymbol();
            return true;
        }
        return false;
    }

    public void setCurrentPlayer(Player currentPlayer) {
        this.currentPlayer = currentPlayer;
    }
    
    public boolean checkWin() {
        if(checkRow()||checkCol()||checkDiagonal1()||checkDiagonal2()){
            return true;
        }
        return false;
    }
    
    private boolean checkRow(){
        for (int i = 0; i < 3; i++) {
            if (board[i][0] == currentPlayer.getSymbol() && board[i][1] == currentPlayer.getSymbol() && board[i][2] == currentPlayer.getSymbol()) {
                return true;
            }
        }
        return false ;
    }   
    
    private boolean checkCol(){
        for (int i = 0; i < 3; i++) {
            if (board[0][i] == currentPlayer.getSymbol() && board[1][i] == currentPlayer.getSymbol() && board[2][i] == currentPlayer.getSymbol()) {
                return true;
            }
        }
        return false;
    }

    private boolean checkDiagonal1() {
        if(board[0][0] == currentPlayer.getSymbol()  && board[1][1] == currentPlayer.getSymbol() && board[2][2] == currentPlayer.getSymbol() ){
            return true;     
        }
        return false;
    }

    private boolean checkDiagonal2() {
        if(board[0][2]== currentPlayer.getSymbol() && board[1][1]== currentPlayer.getSymbol() && board[2][0] == currentPlayer.getSymbol()){
            return true;  
        }
        return false;
    }
    
    public boolean checkDraw(){
     if (checkWin()) {
        return false; 
    }
  
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            if (board[i][j] == '-') {
                return false; 
            }
        }
    }
  
    return true; 
    }
    
    public void saveWin(){
        if(currentPlayer==player1){
            player1.win();
            player2.lose();
        }else{
            player1.lose();
            player2.win();
        } 
    }
    
    public void saveDraw(){
        player1.draw();
        player2.draw();
    }

}
