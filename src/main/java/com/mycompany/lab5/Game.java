/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab5;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Game {
    private Player player1,player2;
    private Board board;

    public Game() {
        player1 = new Player('X');
        player2 = new Player('O');
    }
    
    public void play(){
        boolean isFinish = false;
        newGame();
        printWelcome();
        while(!isFinish){
            printBoard();
            printTurn();
            inputMove();
            if(board.checkWin()){
                printBoard();
                System.out.println("Congratulations! Player "+board.getCurrentPlayer().getSymbol()+" wins!");
                board.saveWin();
                System.out.println("");
                isFinish = true;
            }else if(board.checkDraw()){
                printBoard();
                 System.out.println("The result is Draw!");
                 board.saveDraw();
                isFinish = true;
            }
            switchPlayer();
        }
        printResult();
        playAgain();
    }

    private void printWelcome() {
        System.out.println("|--------------------|");
        System.out.println("| Welcome To OX Game |");
        System.out.println("|--------------------|");
    }
    
    private void printBoard(){
        char[][] board = this.board.getBoard();
        System.out.println("-------------");
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " | ");
            }
            System.out.println();
            System.out.println("-------------");
        }
    }
    
    private void printTurn(){
        System.out.println("Player "+board.getCurrentPlayer().getSymbol()+" Turn");
    }
    
    private void inputMove() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input row [1-3] and column [1-3] for your move [ex:1 1] : ");
        int row = sc.nextInt()-1;
        int col = sc.nextInt()-1;
        if(!board.setBoard(row, col)){
            System.out.println("Invalid move Please try agian.");
            inputMove();
        }
    }
    
    public void newGame(){
       board = new Board(player1, player2);
    }
    
    private void switchPlayer(){
        board.setCurrentPlayer(board.getCurrentPlayer()== player1 ? player2 : player1);
    }
    
    private void playAgain(){
        Scanner sc  = new Scanner(System.in);
        System.out.print("Do you want to play again? (y/n) : ");
        String playAgain = sc.next();
        if(playAgain.equalsIgnoreCase("y")){
            play();
        }else if(playAgain.equalsIgnoreCase("n")){
            System.out.println("Thank you for playing! Goodbye!");
        }
    }
    
    private void printResult(){
        System.out.println(player1);
        System.out.println(player2);
    }
}
