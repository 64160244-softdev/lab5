/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package com.mycompany.lab5;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author thanc
 */
public class Main extends javax.swing.JFrame {
    
    /**
     * Creates new form Main
     */
    public Main() {
        initComponents();
        this.player1 = new Player('X');
        this.player2 = new Player('O');
        DisplayLabel.setText("!!! Welcome To OX Game !!!");
        disableBoard();
        load();
        showResult();
    }
    public void save(){
        FileOutputStream fos = null;
        try {
            File file = new File("player.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(player1);
            oos.writeObject(player2);
            oos.close();
            fos.close();
        } catch (Exception ex) {
            System.out.println("Write Error!!!");
        } finally {
            try {
                if(fos!=null){
                    fos.close();
                }
            }catch(IOException ex){
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void load(){
        FileInputStream fis = null;
        try {
            File file = new File("player.dat");
            fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            player1 = (Player) ois.readObject();
            player2 = (Player) ois.readObject();
            ois.close();
            fis.close();
        } catch (Exception ex) {
            System.out.println("Load Error!!!");
        } finally {
            try {
                if(fis!=null){
                    fis.close();
                }
            }catch(IOException ex){
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void play(){
        enableBoard();
        DisplayLabel.setText("!!! Have A Good Luck !!!");
        playBtn.setEnabled(false);
        playBtn.setText("Now Playing");
        newGame();
        showBoard();
    }
    
    public void disableBoard(){
        Board11.setEnabled(false);
        Board12.setEnabled(false);
        Board13.setEnabled(false);
        Board21.setEnabled(false);
        Board22.setEnabled(false);
        Board23.setEnabled(false);
        Board31.setEnabled(false);
        Board32.setEnabled(false);
        Board33.setEnabled(false);
    }
    
    public void enableBoard(){
        Board11.setEnabled(true);
        Board12.setEnabled(true);
        Board13.setEnabled(true);
        Board21.setEnabled(true);
        Board22.setEnabled(true);
        Board23.setEnabled(true);
        Board31.setEnabled(true);
        Board32.setEnabled(true);
        Board33.setEnabled(true);
    }
    
    public void switchPlayer(){
       board.setCurrentPlayer(board.getCurrentPlayer()== player1 ? player2 : player1);
    }
    
    public void showTurn(){
       DisplayLabel.setText("Player "+board.getCurrentPlayer().getSymbol() + " Turn");
    }
    
    public void showWelcome(){
        DisplayLabel.setText("!!! Welcome To OX Game !!!");
    }
    
    public void newGame(){
       board = new Board(player1, player2);
    }
    
    public void showBoard(){
        char[][] board = this.board.getBoard();
        Board11.setText(board[0][0]+"");
        Board12.setText(board[0][1]+"");
        Board13.setText(board[0][2]+"");
        Board21.setText(board[1][0]+"");
        Board22.setText(board[1][1]+"");
        Board23.setText(board[1][2]+"");
        Board31.setText(board[2][0]+"");
        Board32.setText(board[2][1]+"");
        Board33.setText(board[2][2]+"");
    }
    
    public void process(){
        board.setBoard(row, col);
        showBoard();
        if(board.checkWin()){         
            DisplayLabel.setText("Congratulations! Player "+board.getCurrentPlayer().getSymbol()+" wins!");
            board.saveWin();
            disableBoard();
            save();
            showResult();
            playBtn.setEnabled(true);
            playBtn.setText("New Game");
            
        }else if(board.checkDraw()){             
            DisplayLabel.setText("The result is Draw!");
            board.saveDraw();
            disableBoard();
            save();
            showResult();
            playBtn.setEnabled(true);
            playBtn.setText("New Game");
        }
        switchPlayer();    
    }
    
    public void showResult(){
        playerXWinCount.setText(player1.getWinCount()+"");
        playerXLoseCount.setText(player1.getLoseCount()+"");
        playerXDrawCount.setText(player1.getDrawCount()+"");
        playerOWinCount.setText(player2.getWinCount()+"");
        playerOLoseCount.setText(player2.getLoseCount()+"");
        playerODrawCount.setText(player2.getDrawCount()+"");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        DisplayPanel = new javax.swing.JPanel();
        DisplayLabel = new javax.swing.JLabel();
        BoardPanel = new javax.swing.JPanel();
        Board11 = new javax.swing.JButton();
        Board12 = new javax.swing.JButton();
        Board13 = new javax.swing.JButton();
        Board21 = new javax.swing.JButton();
        Board22 = new javax.swing.JButton();
        Board23 = new javax.swing.JButton();
        Board31 = new javax.swing.JButton();
        Board32 = new javax.swing.JButton();
        Board33 = new javax.swing.JButton();
        OperationPanel = new javax.swing.JPanel();
        playBtn = new javax.swing.JButton();
        playerXPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        playerXWinCount = new javax.swing.JLabel();
        playerXLoseCount = new javax.swing.JLabel();
        playerXDrawCount = new javax.swing.JLabel();
        playerOPanel = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        playerOWinCount = new javax.swing.JLabel();
        playerOLoseCount = new javax.swing.JLabel();
        playerODrawCount = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));

        DisplayPanel.setBackground(new java.awt.Color(255, 255, 255));
        DisplayPanel.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N

        DisplayLabel.setFont(new java.awt.Font("Segoe UI", 0, 30)); // NOI18N
        DisplayLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        DisplayLabel.setText("OX Game");

        javax.swing.GroupLayout DisplayPanelLayout = new javax.swing.GroupLayout(DisplayPanel);
        DisplayPanel.setLayout(DisplayPanelLayout);
        DisplayPanelLayout.setHorizontalGroup(
            DisplayPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(DisplayPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(DisplayLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        DisplayPanelLayout.setVerticalGroup(
            DisplayPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, DisplayPanelLayout.createSequentialGroup()
                .addContainerGap(22, Short.MAX_VALUE)
                .addComponent(DisplayLabel)
                .addGap(14, 14, 14))
        );

        BoardPanel.setBackground(new java.awt.Color(255, 255, 255));

        Board11.setFont(new java.awt.Font("Angsana New", 1, 48)); // NOI18N
        Board11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Board11ActionPerformed(evt);
            }
        });

        Board12.setFont(new java.awt.Font("Angsana New", 1, 48)); // NOI18N
        Board12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Board12ActionPerformed(evt);
            }
        });

        Board13.setFont(new java.awt.Font("Angsana New", 1, 48)); // NOI18N
        Board13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Board13ActionPerformed(evt);
            }
        });

        Board21.setFont(new java.awt.Font("Angsana New", 1, 48)); // NOI18N
        Board21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Board21ActionPerformed(evt);
            }
        });

        Board22.setFont(new java.awt.Font("Angsana New", 1, 48)); // NOI18N
        Board22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Board22ActionPerformed(evt);
            }
        });

        Board23.setFont(new java.awt.Font("Angsana New", 1, 48)); // NOI18N
        Board23.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Board23ActionPerformed(evt);
            }
        });

        Board31.setFont(new java.awt.Font("Angsana New", 1, 48)); // NOI18N
        Board31.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Board31ActionPerformed(evt);
            }
        });

        Board32.setFont(new java.awt.Font("Angsana New", 1, 48)); // NOI18N
        Board32.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Board32ActionPerformed(evt);
            }
        });

        Board33.setFont(new java.awt.Font("Angsana New", 1, 48)); // NOI18N
        Board33.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Board33ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout BoardPanelLayout = new javax.swing.GroupLayout(BoardPanel);
        BoardPanel.setLayout(BoardPanelLayout);
        BoardPanelLayout.setHorizontalGroup(
            BoardPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BoardPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(BoardPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(BoardPanelLayout.createSequentialGroup()
                        .addComponent(Board21, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Board22, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Board23, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(BoardPanelLayout.createSequentialGroup()
                        .addComponent(Board31, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Board32, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Board33, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(BoardPanelLayout.createSequentialGroup()
                        .addComponent(Board11, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Board12, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Board13, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        BoardPanelLayout.setVerticalGroup(
            BoardPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BoardPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(BoardPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Board11, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Board12, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Board13, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(BoardPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Board21, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(BoardPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(Board22, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(Board23, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(BoardPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Board31, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Board32, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Board33, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        OperationPanel.setBackground(new java.awt.Color(255, 255, 255));

        playBtn.setFont(new java.awt.Font("Segoe UI", 0, 32)); // NOI18N
        playBtn.setText("New Game");
        playBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                playBtnActionPerformed(evt);
            }
        });

        playerXPanel.setBackground(new java.awt.Color(255, 102, 102));

        jLabel1.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Player X");

        jLabel3.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Win :");

        jLabel4.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Lose :");

        jLabel5.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Draw :");

        playerXWinCount.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        playerXWinCount.setForeground(new java.awt.Color(255, 255, 255));
        playerXWinCount.setText("0");

        playerXLoseCount.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        playerXLoseCount.setForeground(new java.awt.Color(255, 255, 255));
        playerXLoseCount.setText("0");

        playerXDrawCount.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        playerXDrawCount.setForeground(new java.awt.Color(255, 255, 255));
        playerXDrawCount.setText("0");

        javax.swing.GroupLayout playerXPanelLayout = new javax.swing.GroupLayout(playerXPanel);
        playerXPanel.setLayout(playerXPanelLayout);
        playerXPanelLayout.setHorizontalGroup(
            playerXPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(playerXPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(playerXPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(playerXPanelLayout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(playerXDrawCount))
                    .addGroup(playerXPanelLayout.createSequentialGroup()
                        .addGroup(playerXPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(playerXPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(playerXLoseCount)
                            .addComponent(playerXWinCount))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        playerXPanelLayout.setVerticalGroup(
            playerXPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(playerXPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(playerXPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(playerXWinCount))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(playerXPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(playerXLoseCount))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(playerXPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(playerXDrawCount))
                .addContainerGap())
        );

        playerOPanel.setBackground(new java.awt.Color(102, 153, 255));

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Player O");

        jLabel6.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Win :");

        jLabel7.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Lose :");

        jLabel8.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Draw :");

        playerOWinCount.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        playerOWinCount.setForeground(new java.awt.Color(255, 255, 255));
        playerOWinCount.setText("0");

        playerOLoseCount.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        playerOLoseCount.setForeground(new java.awt.Color(255, 255, 255));
        playerOLoseCount.setText("0");

        playerODrawCount.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        playerODrawCount.setForeground(new java.awt.Color(255, 255, 255));
        playerODrawCount.setText("0");

        javax.swing.GroupLayout playerOPanelLayout = new javax.swing.GroupLayout(playerOPanel);
        playerOPanel.setLayout(playerOPanelLayout);
        playerOPanelLayout.setHorizontalGroup(
            playerOPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(playerOPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(playerOPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(playerOPanelLayout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(playerOWinCount))
                    .addGroup(playerOPanelLayout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(playerOLoseCount))
                    .addGroup(playerOPanelLayout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(playerODrawCount)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        playerOPanelLayout.setVerticalGroup(
            playerOPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(playerOPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(playerOPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(playerOWinCount))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(playerOPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(playerOLoseCount))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(playerOPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(playerODrawCount))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout OperationPanelLayout = new javax.swing.GroupLayout(OperationPanel);
        OperationPanel.setLayout(OperationPanelLayout);
        OperationPanelLayout.setHorizontalGroup(
            OperationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OperationPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(OperationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(playBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 205, Short.MAX_VALUE)
                    .addComponent(playerXPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(playerOPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        OperationPanelLayout.setVerticalGroup(
            OperationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, OperationPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(playerXPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(playerOPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(playBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(DisplayPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(BoardPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(OperationPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(DisplayPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(OperationPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(BoardPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void Board11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Board11ActionPerformed
        // TODO add your handling code here:
        row = 0;
        col = 0;
        process();
    }//GEN-LAST:event_Board11ActionPerformed

    private void playBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_playBtnActionPerformed
        // TODO add your handling code here:
        play();
    }//GEN-LAST:event_playBtnActionPerformed

    private void Board12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Board12ActionPerformed
        // TODO add your handling code here:
        row = 0;
        col = 1;
        process();
    }//GEN-LAST:event_Board12ActionPerformed

    private void Board13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Board13ActionPerformed
        // TODO add your handling code here:
        row = 0;
        col = 2;
        process();
    }//GEN-LAST:event_Board13ActionPerformed

    private void Board21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Board21ActionPerformed
        // TODO add your handling code here:
        row = 1;
        col = 0;
        process();
    }//GEN-LAST:event_Board21ActionPerformed

    private void Board22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Board22ActionPerformed
        // TODO add your handling code here:
        row = 1;
        col = 1;
        process();
    }//GEN-LAST:event_Board22ActionPerformed

    private void Board23ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Board23ActionPerformed
        // TODO add your handling code here:
        row = 1;
        col = 2;
        process();
    }//GEN-LAST:event_Board23ActionPerformed

    private void Board31ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Board31ActionPerformed
        // TODO add your handling code here:
        row = 2;
        col = 0;
        process();
    }//GEN-LAST:event_Board31ActionPerformed

    private void Board32ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Board32ActionPerformed
        // TODO add your handling code here:
        row = 2;
        col = 1;
        process();
    }//GEN-LAST:event_Board32ActionPerformed

    private void Board33ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Board33ActionPerformed
        // TODO add your handling code here:
        row = 2;
        col = 2;
        process();
    }//GEN-LAST:event_Board33ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Board11;
    private javax.swing.JButton Board12;
    private javax.swing.JButton Board13;
    private javax.swing.JButton Board21;
    private javax.swing.JButton Board22;
    private javax.swing.JButton Board23;
    private javax.swing.JButton Board31;
    private javax.swing.JButton Board32;
    private javax.swing.JButton Board33;
    private javax.swing.JPanel BoardPanel;
    private javax.swing.JLabel DisplayLabel;
    private javax.swing.JPanel DisplayPanel;
    private javax.swing.JPanel OperationPanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JButton playBtn;
    private javax.swing.JLabel playerODrawCount;
    private javax.swing.JLabel playerOLoseCount;
    private javax.swing.JPanel playerOPanel;
    private javax.swing.JLabel playerOWinCount;
    private javax.swing.JLabel playerXDrawCount;
    private javax.swing.JLabel playerXLoseCount;
    private javax.swing.JPanel playerXPanel;
    private javax.swing.JLabel playerXWinCount;
    // End of variables declaration//GEN-END:variables
    private Player player1,player2;
    private Board board;
    private int row,col;
}
